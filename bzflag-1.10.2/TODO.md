##to do list for BZFlag
---------------------

###THESE SHOULD HAPPEN BEFORE THE NEXT RELEASE:

1. Name the version *Psyche*

2. Ship it.

3. Notify all those listed in the end of the README

###THESE ARE LOWER PRIORITY

1. Need to use different, clearly disjoint numbering system on bzfs.  perhaps just
  start from 0001 when 1.10 is released.  otherwise it gets unnecessarily bumped to
  match the client version.  it's a separate communication protocol version.

2. server should track last reported player and shot positions

3. Improve server picked spawn locations
  - server needs to track more game state to do this well.

4. remove UDP client setting. code should figure that out now.

5. add other db server features
  * see http://bzflag.org/wiki/DbServers
  * player/password registration
  * open group registration. one nick is master for each group
  * karma see http://bzflag.org/wiki/KarmaSystem

6. Try to build bzadmin on Win32 using PDCurses (it works with the X11
  version of PDCurses). It currently works with stdin/stdout.

7. add ability to capture a screenshot to a gamma-corrected png from
  inside the game.

8. rewrite mac os x display/window code to support app switching,
  resolution mode changes, screen captures, etc.

9. make it so keys can be bound to multiple actions, and provide a way
   to do global scoping on key actions

10. StateDatabase should probably have min max values for eval values

11. split -public <desc> into -public, -description <desc>, and -email <contact>

12. support gameinfo requests as one time udp packets and include all the
  same information that bzfls tracks.  We should allow server info, player and
  score lists over udp from any udp address, not just a "connected" player.

13. Make bcast traffic and serverlist traffic less blocking. (ie: same select() call)

14. listen all the time on udp for bcast game info replies when in Find Servers

15. would be nice if LocalPlayer<->Robots wouldn't relayPackets
  (MsgPlayerUpdate) through server. bzflag should be able to act
  as a relay for a local player and the server should know to
  only send one MsgUpdate to the master, who will relay to the
  connected players/bots. This will allow multiple players
  behind one slow net connection to play multiple players
  behind another slow connection. (for example) ie: -relay

16. bzflag -relay should attempt to listen and reply on udp, resending server
  info for which ever server it is connected to.  Descriptions should begin
  with "relay:" in this case. -solo <n> should enable this behavior.

17. rework the BZDB->eval() caching to support expression
  dependencies - if a variable changes, all the expressions
  that use that variable should be updated as well, or at
  the least, flush the entire cache when a callback happens

18. make the vertical rico solution more elegant - get rid of
  BoxBuilding::getNormal and fix existing code to do z -
  getNormalRect & the like.

19. require an email contact name for public servers. Perhaps
  unpublished by default

20. Create a new MsgThrottle message sent from client to server
  to inform server to throttle MsgPlayerUpdates from other clients
  to X. X is set in bzflag.bzc. Server uses PlayerInfo.lastState to
  batch send PlayerUpdates after throttle time has passed.
  Clients timestamp updates themselves, to catch out of order
  packets, but server restamps timestamps to get consistent
  times for all messages.

21. Lag information should be appended to MsgPlayerUpdate packet
  by server, and use half in dead reckoning calculations

22. remove all DNS lookups from client when contacting a server IP
  supplied from the list server

23. add http proxy support for list servers

24. add http connect proxy support for game servers

25. ev/dsp* on Linux to be selected someplace. command line,
  environment var, config file, who knows. ;-)

26. some bzdb values are stored elsewhere and written to bzdb
  only on exit. these should be stored in bzdb for the entire
  time

27. add caching to bzdb for integer/float values, so they don't
  have to be atoi()'ed or eval()'ed all the time. isTrue would
  also be a good one to cache

28. document headers with JavaDoc-style comments (for doxygen)

29. update doc/protocol.txt all descriptors to new format

30. support gameinfo requests as one time udp packets (if udp)

31. bzadmin should build without GL headers installed fails with:
  ../../include/bzfgl.h:35: GL/gl.h: No such file or directory

32. build a prototype(4) for bsd, solaris

33. fix up irix idb

34. if we stay with tcp/udp then use the same ports for the
   udp pipe as for the tcp pipe on both client and server

35. encapsulate stuff requiring platform #ifdef's:
   networking API into libNet.a.
   fork/exec (used in menus.cxx) into libPlatform.a
   file system stuff (dir delimiter, etc.)
   user name stuff

36. clean up libraries that could be reused by other games

37. move robots to separate binaries and allow either the
   client or server to exec them. Have a server option
   to keep n players in the game which auto spawns/kills
   enough bots to do it. Get rid of bot player type
   completely. bots should get all message types.

38. smarter robots

39. add type of shot (normal, gm, sw, etc) to killed message

40. radio chat:
   allow players to communicate via low-quality audio.
   already some support built into the server for this.

###THESE NEED TO WAIT FOR THE NEXT PROTOCOL BREAKAGE

1. move -synctime data to different Msg packet, or create new one.
  MsgSetVar (ala 1.8) would be a good one

2. flags should all be nullflags from client view until
  a) you pick one up
  b) you get the identify flag (send updates for all flags?)
  If player drops a flag and it stays, players have it's real ID.

3. shorten laser to like 1.25 normal shot
  This is needed for larger maps so we can send sparse player updates.

4. server should send less frequent updates for "distant" players

5. move team base packing into WorldInfo (bzfs)
  and add height (z)

6. remove flipz from pyr net code and replace with neg height.

7. remove shoothrough/drivethrough and replace with one flag
  meaning passable

8. ipv6 support

9. consider using ENet http://enet.cubik.org/ for
   the network protocol

10. implement texture cache on client and have server offer
   new textures as part of the world.

11. allow loadable meshes as "tanks"

12. supply tank "meshes" from the server

13. allow client to choose a mesh from the server list

14. implement one way cross server teleporters

15. implement a visual "server list" game server

16. pre-game waiting room:
   provide a means for players to gather before and after
   a game.  basic text chat facilities.  allows players
   to wait for enough players for a game and to discuss
   strategy, etc. **could build this into bzfs easily**

