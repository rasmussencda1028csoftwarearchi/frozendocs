##Project FrozenFist
Contact: [Evan Jungwirth](evan.jungwirth@smail.rasmussen.edu)

####VortexOps

You have just been hired as a summer intern at VortexOps. You've been assigned to work with the software development team, assisting the software architect.

The team is currently working on a project codenamed "Frozen Fist." They are designing software for semi-autonomous armored assault vehicles.
The vehicles are designed to operate either remotely with a human pilot or in "robot mode" with guidance from an on-board Artificial Intelligence (AI) system.

Your job will consist of helping the team with planning, organizing, and designing their latest application project. 
In short, you will be doing the things that don't involve writing code, so as to free up their highly paid software developers. 
In the meantime, you will get to see how the software design and development project works first-hand.

### Modified README on BitBucket - 1/21/2018
